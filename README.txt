# README # 
### What is this repository for? ### 

Test program for Nitro. 
Object of the project: evaluate a complete list of the intersections between two or more rectangles described in a given input .json file
For detailed specifics see document C___Proficiency_test_for_Senior_Candidates.pdf 

### How do I get set up? ### 

**** MAIN PROGRAM ******

The main program, testNitro.exe, was compiled with the Microsoft Visual Studio IDE downloaded at
https://www.visualstudio.com/free-developer-offers/

Open the solution testNitro.sln with Microsoft Visual Studio and compile by using "Build/Build solution".
The resulting program testNitro.exe will be created under the detination "Debug" folder.

## How do I test it? ### 
The program comes with a test suite of automatic tests, plus some input files prepared to test some specific situations,
and of course you can try and execute it with any input you like!

**** TEST SUITE *****
The InputTest1Nitro solution implements a series of tests on some of the functionality of the program 

Open the solution testNitro.sln with Microsoft Visual Studio and execute all tests by "Ctrl R, a" or Menu Test->Run->All Tests
All tests in unit test should complete with success (green bulletsyy)


**** MANUAL EXECUTION *****
To execute the program, open a command shell and execute it.
As with any other command, either position yourself in the directory containing the program, 
or add the path to the system $PATH variable, or explicitly write the execution path for the 
program.
usage: testNitro.exe <inputFileName>


**** SAMPLE INPUT FILES FOR MANUAL TESTS ****
The directory "TestFiles" contains some input files prepared to test the program:
- fail_1.json, fail_2.json, fail_3.json, fail_4.json, fail_5.json
   these files all contain not-well formatted input. Expected behaviour: program exits with error message, reporting the location of the error
- rectangles_1.json, rectangles_2.json, rectangles_3.json  
   these files all contain well formatted input. Expected behaviour: program actually produces the intersections list as output
- touchingBorders.json
   this file tests the particular case of rectangles who are adjacent. This would produce intersections of width or height 0
   Expected behaviour: program produces an empty list of intersections. All resulting interceptions would have either w=0 or y=0 or both.
- lotsOfRectangles.json 
   this file contains a well formatted input with 1000+ rectangles, but with a limited number of intersections. Expected behaviour: program actually produces the intersections list as output
- worstCase.json
   this file contains a well formatted input with 1000+ rectangles, all identical, which would produce a number of resulting intersections in the order of (2^1000). Expected behaviour: program actually produces list, but goes on forever... this test was produced to check that the program does not cause out of memory errors. Stop execution after a reasonable ;) amount of time.  
   