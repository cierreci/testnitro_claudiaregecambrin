#include <vector>
#include <string>
#include <algorithm>

using namespace std;

#include "rectangle.h"

string Rectangle::printBasic()
{
	return string() + "\t" + to_string(rectangles[0]) + ": Rectangle at (" +
		to_string(x) + "," + to_string(y) + "), w=" + to_string(w) + ", h=" + to_string(h) + ".";
}

string Rectangle::get_intersection_list()
{
	string result = string();
	for (size_t i = 0; i < rectangles.size(); i++) {
		unsigned int id = rectangles[i];
		result += to_string(rectangles[i]);
		if (rectangles.size()>2 && i<rectangles.size() - 2)
			result += ", ";
		else if (i == rectangles.size() - 2)
			result += " and ";
	}
	return result;
}

string Rectangle::printIntersection()
{
	return string() + "\tBetween rectangle " + get_intersection_list() + " at (" +
		to_string(x) + "," + to_string(y) + "), w=" + to_string(w) + ", h=" + to_string(h) +  ".";
}

bool Rectangle::getIntersection(const Rectangle &other, Rectangle &result)
{
	/* two rectangles have an intersection if the segments defined by their abscissas intersect AND so do the segments of their ordinates.
	two segments do NOT intesect if one end before the other starts:
	segment A1-B1 and A2-B2 do not intersect if B2 < A1 || B1 < A2
	*/

#ifdef BORDERS_DO_NOT_INTERSECT
	// use this version if you do not want intersections of width 0
	if (x + w <= other.x || other.x + other.w <= x)
		return false;

	if (y + h <= other.y || other.y + other.h <= y)
		return false;
#else
	if (x + w < other.x || other.x + other.w < x)
		return false;

	if (y + h < other.y || other.y + other.h < y)
		return false;
#endif 

	result.x = max(x, other.x);
	result.w = min(x + w, other.x + other.w) - result.x;

	result.y = max(y, other.y);
	result.h = min(y + h, other.y + other.h) - result.y;

	result.rectangles = rectangles;
	result.rectangles.push_back(other.rectangles[0]);
	return true;
}

