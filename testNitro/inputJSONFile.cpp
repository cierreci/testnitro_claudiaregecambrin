#include <vector>
#include <string>
#include <iostream>
#include <fstream>

using namespace std; 
#include "rectangle.h"
#include "inputJSONFile.h"

void inputJSONFile::skipBlanks()
{
	int ch = inputfile.peek();
	while (isblank(ch) /*|| ch == '\t'*/ || ch == '\n') {
		(void)inputfile.get();
		if (ch == '\n')
			currentLine++;
		ch = inputfile.peek();
	}
}

void inputJSONFile::getString(const string expectedString)
{
	skipBlanks();
	for (size_t i = 0; i < expectedString.size(); i++) {
		if (inputfile.get() != expectedString[i]) {
			throw string() + "Invalid format at line " + to_string(currentLine) + ": expected " + expectedString;
		}
	}
}

void inputJSONFile::getIntValue(int &val, const char*valName)
{
	double dval;
	inputfile >> dval;
	if (inputfile.fail()) {
		throw string() + "Invalid format at line " + to_string(currentLine) + ": expected a well-formatted integer for " + valName;
	}
	double intpart;
	if (modf(dval, &intpart) != 0.0) {
		throw string() + "Invalid format at line " + to_string(currentLine) + ": expected an integer value for " + valName;
	}
	if (dval < INT_MIN || dval > INT_MAX) {
		throw string() + "Invalid format at line " + to_string(currentLine) + ": value for " + valName + " exceeds legitimate range";
	}
	val = (int)dval;
}

#define MAX_INPUT_RECTANGLES 1000
void inputJSONFile::readRectangle(Rectangle &rect)
{
	int x, y, w, h;
	getString("\"x\"");
	getString(":");
	getIntValue(x, "x");

	getString(",");
	getString("\"y\"");
	getString(":");
	getIntValue(y, "y");

	getString(",");
	getString("\"w\"");
	getString(":");
	getIntValue(w, "w");

	getString(",");
	getString("\"h\"");
	getString(":");
	getIntValue(h, "h");

	if (!(x >= 0 && y >= 0)) {
		throw string() + "Invalid format at line " + to_string(currentLine) + ": expected non negative values for x and y";
	}
	if (!(w>0 && h>0)) {
		throw string() + "Invalid format at line " + to_string(currentLine) + ": expected non negative values for w and h";
	}
	rect.setValues(x, y, w, h);
}

void inputJSONFile::readJSONRectangles(vector<Rectangle> &allRectangles)
{
	/* format:
	{
	"rects" : [
	{"x":vx, "y":vy, "w":vw, "h":vh},
	{"x":vx, "y":vy, "w":vw, "h":vh},
	]
	}
	arbitrary white spaces are allowed between tokens and should be skipped
	*/
	getString("{");
	getString("\"rects\"");
	getString(":");
	getString("[");
	while (1) {
		getString("{");
		Rectangle rect((unsigned int)(allRectangles.size()) + 1);
		readRectangle(rect); // we do the readRectangle even if the array is at maximum size because we want to check that the file is syntactically correct
		if (allRectangles.size() < MAX_INPUT_RECTANGLES) {
			allRectangles.push_back(rect);
		}
		getString("}");
		skipBlanks();
		if (inputfile.peek() != ',')
			break;
		else
			inputfile.get(); // consume the ','
	}
	getString("]");
	getString("}");
	skipBlanks();
	// lest's check we are at ed of file
	if (!inputfile.eof()) {
		throw string() + "Invalid format at line " + to_string(currentLine) + ": unexpected stuff after end of array";
	}
}

void inputJSONFile::open(string filename)
{
	inputfile.open(filename, ifstream::in);
	if (!inputfile.is_open()) {
		throw string() + "Unable to open file " + filename + " for reading";
	}
	currentLine = 1;
}

void inputJSONFile::close()
{
	if (inputfile.is_open()) {
		inputfile.close();
	};
	currentLine = -1;
}

inputJSONFile::~inputJSONFile()
{
	close();
}
