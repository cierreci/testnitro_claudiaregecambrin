#include <iostream>
using namespace std;

#include "testNitro.h"

int main(int argc, char **argv) {
	if (argc != 2) {
		cout << "Usage: " << argv[0] << " filename" << endl;
		return 1;
	}
	int result = analyzeFile(argv[1]);
	return result;
}
