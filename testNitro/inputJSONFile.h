#pragma once

class inputJSONFile {
	ifstream inputfile;
	void skipBlanks();
	void getString(const string expectedString);
	void getIntValue(int &val, const char*valName);
	void readRectangle(Rectangle &rect);
	int currentLine;
public:
	void readJSONRectangles(vector<Rectangle> &allRectangles);
	void open(string filename);
	void close();
	~inputJSONFile();
};

