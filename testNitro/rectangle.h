#pragma once

class Rectangle {
	vector<unsigned int>rectangles; // all the rectangles whose intesection produced this rectangle. 
									// If it is a "native" rectangle (i.e it was in the original list) will only contain its ID;
	unsigned int x, y, w, h;

	string get_intersection_list();

public:
	Rectangle() { x = y = w = h = 0; };
	Rectangle(unsigned int ID) {
		x = y = w = h = 0;
		rectangles.push_back(ID);
	}
	bool getIntersection(const Rectangle &other, Rectangle &result);
	string printBasic();
	string printIntersection();
	inline int getLastIntercepted() { return rectangles.back(); };
	inline void setValues(unsigned int _x, unsigned int _y, unsigned int _w, unsigned int _h) {
		x = _x;
		y = _y;
		w = _w;
		h = _h;
	};
};

#define BORDERS_DO_NOT_INTERSECT

/* uncomment if we do want to consider an intersection of width / height 0 as a valid one
#undef BORDERS_DO_NOT_INTERSECT
*/