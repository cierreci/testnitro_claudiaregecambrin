#include <vector>
#include <string>
#include <iostream>
#include <fstream>
#include <algorithm>
#undef _SOLUTION_TOO_MUCH_MEMORY_
#ifdef _SOLUTION_TOO_MUCH_MEMORY_
#include <deque>
#endif
using namespace std;

#include "rectangle.h"
#include "inputJSONFile.h"

void increment_counter(long long& numIntersections)
{
	/* no point in counting more than... */
	if (numIntersections < 2e63)
		numIntersections++;
}

#ifndef _SOLUTION_TOO_MUCH_MEMORY_
static void tryAllFurtherIntersections(vector<Rectangle> &allRectangles, Rectangle &intersection, long long &numIntersections)
{
	int last_intersected = intersection.getLastIntercepted();
	for (size_t j = last_intersected; j < allRectangles.size(); j++) {
		Rectangle newIntersection;
		if (intersection.getIntersection(allRectangles[j], newIntersection)) {
			increment_counter(numIntersections);
			cout << newIntersection.printIntersection() << endl;
			tryAllFurtherIntersections(allRectangles, newIntersection, numIntersections);
		}
	}
}

long long calculateIntersections(vector<Rectangle> &allRectangles)
{
	size_t original_size = allRectangles.size();
	long long numIntersections = 0;

	/* starting with the original rectangles, for each of them we check if there are valid intersections with one of the FOLLOWING ones.
	no need to check the previous ones or we would get the same intersection twice.
	every valid intersection found can be added at the end of the vector and we will later try to intercept it with more rectangles
	among the ones that haven't been attempted yet (it is sufficient to know which was the LAST rectangle intersecated;
	the ones before do not need to be checked again)
	*/
	for (size_t i = 0; i < original_size; i++) {
		// if allRectangles[i] was the result of an interception, for instance between 1,2 and 4, we do not need to check rectangles before 4 because we already have done it. So we need to check rectangle from 5 to original_size
		int last_intersected = allRectangles[i].getLastIntercepted();
		for (size_t j = last_intersected; j < original_size; j++) {
			Rectangle intersection;
			if (allRectangles[i].getIntersection(allRectangles[j], intersection)) {
				increment_counter(numIntersections);
				cout << intersection.printIntersection() << endl;
				tryAllFurtherIntersections(allRectangles, intersection, numIntersections);
			}
		}
	}
	return numIntersections;
}

#else
/* this solution produces an output in which the intersections are printed in "alfabetical" order
that is, we get first all intersections between 2 drawings, then all the ones between three, and so on.
Sadly, it consumes too much memory: in an ipothetical case of 1000 identical rectangles
it vould allocate a vector if about 2^1000 Rectangle, and no computer has that memory available!

The other solution adopt a "depth first" approach that produces the output in different order,
but since we do not need to save the intersections all the memory needed is the one required for the stack,
whose depth is at most n (n== numbere of rectangles in input)
*/

static long long calculateIntersections(vector<Rectangle> &allRectangles)
{
	size_t original_size = allRectangles.size();
	deque<Rectangle> intersections;
	long long numIntersections;

	/* starting with the original rectangles, for each of them we check if there are valid intersections with one of the FOLLOWING ones.
	no need to check the previous ones or we would get the same intersection twice.
	every valid intersection found can be added at the end of the vector and we will later try to intercept it with more rectangles
	among the ones that haven't been attempted yet (it is sufficient to know which was the LAST rectangle intersecated;
	the ones before do not need to be checked again)
	*/
	for (size_t i = 0; i < allRectangles.size(); i++) {
		// if allRectangles[i] was the result of an interception, for instance between 1,2 and 4, we do not need to check rectangles before 4 because we already have done it. So we need to check rectangle from 5 to original_size
		int last_intersected = allRectangles[i].getLastIntercepted();
		for (size_t j = last_intersected; j < original_size; j++) {
			Rectangle intersection;
			if (allRectangles[i].getIntersection(allRectangles[j], intersection)) {
				increment_counter(numIntersections);
				intersections.push_back(intersection);
			}
		}
	}

	/* and then we try to intersect the intersections with the original rectangles */
	while (!intersections.empty()) {
		Rectangle intersection = intersections[0];
		intersections.pop_front();
		cout << intersection.printIntersection() << endl;
		int last_intersected = intersection.getLastIntercepted();
		for (size_t j = last_intersected; j < original_size; j++) {
			Rectangle newIntersection;
			if (intersection.getIntersection(allRectangles[j], newIntersection)) {
				increment_counter(numIntersections);
				intersections.push_back(newIntersection);
			}
		}
	}
	return numIntersections;
}
#endif _SOLUTION_TOO_MUCH_MEMORY_

static void printIntersections(vector<Rectangle> allRectangles)
{
	// print all original rectangles.
	cout << "Input:" << endl;
	size_t  i = 0;
	for (; i < allRectangles.size(); i++) {
		cout << allRectangles[i].printBasic() << endl;
	}
	cout << endl;

	// elaborate and print all intersections;
	// we could first calculate them and then print; but with many rectangles and many intersections we risk running out of memory 
	cout << "Intersections" << endl;
	calculateIntersections(allRectangles);
}


int analyzeFile(string inputFilename)
{
	vector<Rectangle> allRectangles;
	inputJSONFile inputfile;
	try {
		inputfile.open(inputFilename);
		inputfile.readJSONRectangles(allRectangles);
		inputfile.close();
	}
	catch (string &s) {
		inputfile.close();
		cout << "Error while reading file " << inputFilename << ": " << s << endl;
		return 2;
	}
	catch (...) {
		inputfile.close();
		cout << "Unhandled exception while reading file " << inputFilename << endl;
		return 2;
	}

	try {
		printIntersections(allRectangles);
	}
	catch (...) {
		cout << "Unhandled exception while elaborating answer " << endl;
		return 3;
	}
	return 0;
}


