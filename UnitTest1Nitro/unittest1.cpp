#include "CppUnitTest.h"

#include <vector>
#include <string>
#include <iostream>
#include <fstream>
#include <algorithm>
using namespace std;

#include "rectangle.h"
#include "inputJSONFile.h"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

extern long long calculateIntersections(vector<Rectangle> &allRectangles);

namespace UnitTest1Nitro
{
	TEST_CLASS(test_Rectangle)
	{
	public:

		TEST_METHOD(rectangle_setvalues)
		{
			// check the rectangle constructor
			Rectangle rect(1);
			Assert::AreEqual(rect.getLastIntercepted(), 1);
			// check the printbasic function
			Assert::AreEqual(rect.printBasic(), string() + "\t1: Rectangle at (0,0), w=0, h=0.");

			// check the setValues function
			rect.setValues(10, 10, 10, 10);
			Assert::AreEqual(rect.printBasic(), string() + "\t1: Rectangle at (10,10), w=10, h=10.");
		}

		TEST_METHOD(rectangle_lastintercepted)
		{
			Rectangle rect1(1);
			rect1.setValues(10, 10, 10, 10);
			Rectangle rect2(2);
			rect2.setValues(12, 12, 10, 10);
			Rectangle rect3(3);
			rect3.setValues(30, 30, 10, 10);
			Assert::AreEqual(rect1.getLastIntercepted(), 1);
			Assert::AreEqual(rect2.getLastIntercepted(), 2);
			Assert::AreEqual(rect3.getLastIntercepted(), 3);

		}

		TEST_METHOD(rectangle_intersection)
		{
			Rectangle rect1(1);
			rect1.setValues(10, 10, 10, 10);
			Rectangle rect2(2);
			rect2.setValues(12, 12, 10, 10);
			Rectangle rect3(2);
			rect3.setValues(30, 30, 10, 10);

			Rectangle result;
			// check: rectangle 1 and 2 intersect as 12,12:8,8
			Assert::AreEqual(rect1.getIntersection(rect2, result), true);
			Assert::AreEqual(result.printIntersection(), string() + "\tBetween rectangle 1 and 2 at (12,12), w=8, h=8.");
			Assert::AreEqual(result.getLastIntercepted(), 2);
			// check: rectangle 1 and 3 do not intersect 
			Assert::AreEqual(rect1.getIntersection(rect3, result), false);
		}

		TEST_METHOD(rectangle_intersection_border)
		{
			// test: two rectangles touch each other's border
			Rectangle rect1(1);
			rect1.setValues(100, 100, 100, 100);
			Rectangle rect2(2);
			rect2.setValues(200, 100, 50, 100);

			Rectangle result;
#ifdef BORDERS_DO_NOT_INTERSECT
			// if we do not want intersections of width/height 0, these two rectangles do NOT intersect
			Assert::AreEqual(rect1.getIntersection(rect2, result), false);
#else
			// otherwise these two rectangles do intersect and the result has w=0
			Assert::AreEqual(rect1.getIntersection(rect2, result), true);
			Assert::AreEqual(result.printIntersection(), string() + "\tBetween rectangle 1 and 2 at (200,100), w=0, h=100.");
#endif
		}

		TEST_METHOD(rectangle_intersection_inner)
		{
			// test: one rectangle completely contained in the other
			Rectangle rect1(1);
			rect1.setValues(100, 100, 100, 100);
			Rectangle rect2(2);
			rect2.setValues(120, 120, 50, 50);

			Rectangle result;
			// check: rectangle 1 and 2 intersect as 12,12:8,8
			Assert::AreEqual(rect1.getIntersection(rect2, result), true);
			Assert::AreEqual(result.printIntersection(), string() + "\tBetween rectangle 1 and 2 at (120,120), w=50, h=50.");
		}

		TEST_METHOD(rectangle_intersection_2angle)
		{
			// test: two angles of the first rectangle are contained in the other rectangle
			Rectangle rect1(1);
			rect1.setValues(100, 100, 100, 100);
			Rectangle rect2(2);
			rect2.setValues(120, 80, 60, 100);

			Rectangle result;
			// check: rectangle 1 and 2 intersect as 12,12:8,8
			Assert::AreEqual(rect1.getIntersection(rect2, result), true);
			Assert::AreEqual(result.printIntersection(), string() + "\tBetween rectangle 1 and 2 at (120,100), w=60, h=80.");
		}
	};

	TEST_CLASS(test_inputJSONFile)
	{
	public:

		TEST_METHOD(inputJSONFile_open_ok)
		{
			// test opening of existing file
			string filename = "inputJSONFile.tst.op";
			ofstream ofile;
			ofile.open(filename, ios::trunc);
			Assert::IsTrue(ofile.is_open());
			ofile << "dummy";
			ofile.close();

			inputJSONFile jFile;
			jFile.open(filename);
			jFile.close();
		}

		TEST_METHOD(inputJSONFile_open_ko)
		{
			// test opening of NOT existing file
			string filebase = "inputJSONFile.tst.op";
			string filename;
			bool failed = false;
			int i = 0;
			while (!failed) {
				filename = filebase + to_string(i);
				ifstream tryf;
				tryf.open(filename, ios::in);
				if (!tryf.is_open())
					break;
			}
			try {
				inputJSONFile jFile;
				jFile.open(filename);
				jFile.close();
				Assert::IsTrue(false);
			}
			catch (string & s) {
				Assert::AreEqual(s, string() + "Unable to open file " + filename + " for reading");
			}
		}
	};

	TEST_CLASS(test_inputJSONFile_parse)
	{
	public:

		TEST_METHOD(inputJSONFile_parse_empty)
		{
			//test: an empty file is rejected as not valid
			string filename = "inputJSONFile.tst.op";
			ofstream ofile;
			ofile.open(filename, ios::trunc);
			Assert::IsTrue(ofile.is_open());
			ofile.close();

			try {
				inputJSONFile jFile;
				jFile.open(filename);
				vector<Rectangle> allRect;
				jFile.readJSONRectangles(allRect);
				Assert::IsTrue(false); // should NOT get here!
				jFile.close();
			}
			catch (string & s) {
				Assert::IsTrue(s.find("Invalid format") != string::npos);
			}
		};

		TEST_METHOD(inputJSONFile_parse_missing)
		{
			//test: a file without required data is rejected as not valid
			string filename = "inputJSONFile.tst.op";
			ofstream ofile;
			ofile.open(filename, ios::trunc);
			Assert::IsTrue(ofile.is_open());
			ofile << "{\"x\":1000, \"y\":20, 					}      " << endl; // missing w,h
			ofile << "]                                                                  " << endl;
			ofile << "}                                                                  " << endl;
			ofile.close();

			try {
				inputJSONFile jFile;
				jFile.open(filename);
				vector<Rectangle> allRect;
				jFile.readJSONRectangles(allRect);
				Assert::IsTrue(false); // should NOT get here!
				jFile.close();
			}
			catch (string & s) {
				Assert::IsTrue(s.find("Invalid format") != string::npos);
			}
		};

		TEST_METHOD(inputJSONFile_parse_comma)
		{
			//test: a file with valid array but with a ',' at the end is rejected as not valid
			string filename = "inputJSONFile.tst.op";
			ofstream ofile;
			ofile.open(filename, ios::trunc);
			Assert::IsTrue(ofile.is_open());
			ofile << "{ \"rects\" : [                                                    " << endl;
			ofile << "{\"x\":100, \"y\":100, 					\"w\":250, \"h\":80},      " << endl;
			ofile << "]                                                                  " << endl;
			ofile << "}                                                                  " << endl;
			ofile.close();

			try {
				inputJSONFile jFile;
				jFile.open(filename);
				vector<Rectangle> allRect;
				jFile.readJSONRectangles(allRect);
				Assert::IsTrue(false); // should NOT get here!
				jFile.close();
			}
			catch (string & s) {
				Assert::IsTrue(s.find("Invalid format") != string::npos);
			}
		};

		TEST_METHOD(inputJSONFile_parse_extra)
		{
			//test: a file with valid array but with extra stuff at the end is rejected as not valid
			string filename = "inputJSONFile.tst.op";
			ofstream ofile;
			ofile.open(filename, ios::trunc);
			Assert::IsTrue(ofile.is_open());
			ofile << "{ \"rects\" : [                                                    " << endl;
			ofile << "{\"x\":100, \"y\":100, 					\"w\":250, \"h\":80},      " << endl;
			ofile << "]                                                                  " << endl;
			ofile << "}                                                                  " << endl;
			ofile << "This line should not be here!" << endl;
			ofile.close();

			try {
				inputJSONFile jFile;
				jFile.open(filename);
				vector<Rectangle> allRect;
				jFile.readJSONRectangles(allRect);
				Assert::IsTrue(false); // should NOT get here!
				jFile.close();
			}
			catch (string & s) {
				Assert::IsTrue(s.find("Invalid format") != string::npos);
			}
		};

		TEST_METHOD(inputJSONFile_parse_ok)
		{
			//test: a file with valid array is regularly parsed
			string filename = "inputJSONFile.tst.op";
			ofstream ofile;
			ofile.open(filename, ios::trunc);
			Assert::IsTrue(ofile.is_open());
			ofile << "{ \"rects\" : [                                                    " << endl;
			ofile << "{\"x\":100, \"y\":100, 					\"w\":250, \"h\":80},      " << endl;
			ofile << "{\"x\":120, \"y\":200, 					\"w\":250, \"h\":150},     " << endl;
			ofile << "{\"x\":140, \"y\":160, 					\"w\":250, \"h\":100},     " << endl;
			ofile << "{\"x\":160, \"y\":140, 					\"w\":350, \"h\":190},     " << endl;
			ofile << "{\"x\":1020, \"y\":0, 					\"w\":960, \"h\":1000},    " << endl;
			ofile << "{\"x\":1000, \"y\":20, 					\"w\":100, \"h\":960}      " << endl;
			ofile << "]                                                                  " << endl;
			ofile << "}                                                                  " << endl;	
			ofile.close();

			{
				inputJSONFile jFile;
				jFile.open(filename);
				vector<Rectangle> allRect;
				jFile.readJSONRectangles(allRect);
				jFile.close();
				Assert::IsTrue(allRect.size() == 6);
			}
		};

		TEST_METHOD(inputJSONFile_parse_checkint)
		{
			//test: a file with valid array but with non-integer values is rejected
			string filename = "inputJSONFile.tst.op";
			ofstream ofile;
			ofile.open(filename, ios::trunc);
			Assert::IsTrue(ofile.is_open());
			ofile << "{ \"rects\" : [                                                    " << endl;
			ofile << "{\"x\":100.5, \"y\":100, 					\"w\":250, \"h\":80},      " << endl;
			ofile << "]                                                                  " << endl;
			ofile << "}                                                                  " << endl;
			ofile.close();

			try {
				inputJSONFile jFile;
				jFile.open(filename);
				vector<Rectangle> allRect;
				jFile.readJSONRectangles(allRect);
				Assert::IsTrue(false); // should NOT get here!
				jFile.close();
			}
			catch (string & s) {
				Assert::IsTrue(s.find("Invalid format") != string::npos);
			}
		};

		TEST_METHOD(inputJSONFile_parse_overflow)
		{
			//test: a file with valid array but with out-of-range values is rejected
			string filename = "inputJSONFile.tst.op";
			ofstream ofile;
			ofile.open(filename, ios::trunc);
			Assert::IsTrue(ofile.is_open());
			ofile << "{ \"rects\" : [                                                    " << endl;
			ofile << "{\"x\":100e78, \"y\":100, 					\"w\":250, \"h\":80},      " << endl;
			ofile << "]                                                                  " << endl;
			ofile << "}                                                                  " << endl;
			ofile.close();

			try {
				inputJSONFile jFile;
				jFile.open(filename);
				vector<Rectangle> allRect;
				jFile.readJSONRectangles(allRect);
				Assert::IsTrue(false); // should NOT get here!
				jFile.close();
			}
			catch (string & s) {
				Assert::IsTrue(s.find("Invalid format") != string::npos);
			}
		};

		TEST_METHOD(inputJSONFile_parse_exponential)
		{
			//test: a file with valid array and integer values in exponential format is regularly parsed
			string filename = "inputJSONFile.tst.op";
			ofstream ofile;
			ofile.open(filename, ios::trunc);
			Assert::IsTrue(ofile.is_open());
			ofile << "{ \"rects\" : [                                                    " << endl;
			ofile << "{\"x\":100e2, \"y\":100, 					\"w\":250e3, \"h\":80},      " << endl;
			ofile << "{\"x\":120e2, \"y\":200, 					\"w\":250e3, \"h\":150},     " << endl;
			ofile << "{\"x\":140e3, \"y\":160, 					\"w\":250e3, \"h\":100},     " << endl;
			ofile << "{\"x\":160e2, \"y\":140, 					\"w\":350e3, \"h\":190},     " << endl;
			ofile << "{\"x\":1020e2, \"y\":0, 					\"w\":960e3, \"h\":1000},    " << endl;
			ofile << "{\"x\":1000e3, \"y\":20, 					\"w\":100e3, \"h\":960}      " << endl;
			ofile << "]                                                                  " << endl;
			ofile << "}                                                                  " << endl;
			ofile.close();

			{
				inputJSONFile jFile;
				jFile.open(filename);
				vector<Rectangle> allRect;
				jFile.readJSONRectangles(allRect);
				jFile.close();
				Assert::IsTrue(allRect.size() == 6);
			}
		};
	};

	TEST_CLASS(test_calculateIntersections)
	{
	public:

		TEST_METHOD(calculateIntersections_example1)
		{
			//test: this is the actual example in the test description
			string filename = "inputJSONFile.tst.op";
			ofstream ofile;
			ofile.open(filename, ios::trunc);
			Assert::IsTrue(ofile.is_open());
			ofile << "{ \"rects\" : [                                                    " << endl;
			ofile << "{\"x\":100, \"y\":100, 					\"w\":250, \"h\":80},      " << endl;
			ofile << "{\"x\":120, \"y\":200, 					\"w\":250, \"h\":150},     " << endl;
			ofile << "{\"x\":140, \"y\":160, 					\"w\":250, \"h\":100},     " << endl;
			ofile << "{\"x\":160, \"y\":140, 					\"w\":350, \"h\":190}     " << endl;
			ofile << "]                                                                  " << endl;
			ofile << "}                                                                  " << endl;
			ofile.close();

			{
				inputJSONFile jFile;
				jFile.open(filename);
				vector<Rectangle> allRect;
				jFile.readJSONRectangles(allRect);
				jFile.close();
				Assert::IsTrue(allRect.size() == 4);
				long long numIntersections = calculateIntersections(allRect);
				Assert::IsTrue(numIntersections == 7);
			}
		};
	};
}